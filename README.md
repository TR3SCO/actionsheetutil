# ActionSheetUtil

[![CI Status](http://img.shields.io/travis/carlos@tr3sco.com/ActionSheetUtil.svg?style=flat)](https://travis-ci.org/carlos@tr3sco.com/ActionSheetUtil)
[![Version](https://img.shields.io/cocoapods/v/ActionSheetUtil.svg?style=flat)](http://cocoapods.org/pods/ActionSheetUtil)
[![License](https://img.shields.io/cocoapods/l/ActionSheetUtil.svg?style=flat)](http://cocoapods.org/pods/ActionSheetUtil)
[![Platform](https://img.shields.io/cocoapods/p/ActionSheetUtil.svg?style=flat)](http://cocoapods.org/pods/ActionSheetUtil)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ActionSheetUtil is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "ActionSheetUtil"
```

## Author

carlos@tr3sco.com, carlos@tr3sco.com

## License

ActionSheetUtil is available under the MIT license. See the LICENSE file for more info.
