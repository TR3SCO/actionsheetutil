#
# Be sure to run `pod lib lint ActionSheetUtil.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'ActionSheetUtil'
  s.version          = '0.1.0'
  s.summary          = 'Action sheet With a handler block parameter'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
Action sheet Without a delegate, it uses a handler black instead
                       DESC

  s.homepage         = 'https://bitbucket.org/TR3SCO/actionsheetutil'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Carlos Martinez' => 'carlos@tr3sco.com' }
  s.source           = { :git => 'https://jkarlosmtzs@bitbucket.org/TR3SCO/actionsheetutil.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'ActionSheetUtil/Classes/**/*'
  
  # s.resource_bundles = {
  #   'ActionSheetUtil' => ['ActionSheetUtil/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
    s.frameworks = 'UIKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
