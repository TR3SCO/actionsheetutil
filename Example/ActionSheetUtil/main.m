//
//  main.m
//  ActionSheetUtil
//
//  Created by carlos@tr3sco.com on 08/21/2017.
//  Copyright (c) 2017 carlos@tr3sco.com. All rights reserved.
//

@import UIKit;
#import "T3AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([T3AppDelegate class]));
    }
}
