//
//  T3AppDelegate.h
//  ActionSheetUtil
//
//  Created by carlos@tr3sco.com on 08/21/2017.
//  Copyright (c) 2017 carlos@tr3sco.com. All rights reserved.
//

@import UIKit;

@interface T3AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
