//
//  UIActionSheet+Utils.m
//
//  Created by José Carlos Martínez Sandoval on 07/07/15.
//  Copyright (c) 2015 Tr3sco. All rights reserved.
//

#import "UIActionSheet+Utils.h"

#import <objc/runtime.h>

@implementation UIActionSheet (Utils)

@dynamic handlerSheet;


-(void)setHandlerSheet:(completionSheet)handlerSheet
{
    objc_setAssociatedObject(self, @selector(handlerSheet), handlerSheet, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(completionSheet)handlerSheet
{
    return objc_getAssociatedObject(self, @selector(handlerSheet));
}

-(void)showInView:(UIView *)view handler:(completionSheet)handler
{
    self.handlerSheet = handler;
    self.delegate = self;
    [self showInView:view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    self.handlerSheet(buttonIndex);
    self.handlerSheet = nil;
}

@end
