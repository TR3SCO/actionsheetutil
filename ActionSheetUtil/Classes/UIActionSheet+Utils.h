//
//  UIActionSheet+Utils.h
//
//  Created by José Carlos Martínez Sandoval on 07/07/15.
//  Copyright (c) 2015 Tr3sco. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  Typedef for completion sheet.
 *
 *  @param indexButton Index button.
 */
typedef void (^completionSheet)(NSInteger indexButton);

/**
 *  Action sheet category.
 */
@interface UIActionSheet (Utils) <UIActionSheetDelegate>

/**
 *  The handler sheet.
 */
@property (strong, nonatomic) completionSheet handlerSheet;

/**
 *  Show a completion sheet via delegate.
 *
 *  @param view    The view where completion sheet will be display.
 *  @param handler The delegate handler.
 */
-(void)showInView:(UIView *)view handler:(completionSheet)handler;

@end
